import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LangComponent {
    private String name;
    private String translate;
}
