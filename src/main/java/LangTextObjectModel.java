import lombok.Data;

import java.util.ArrayList;

@Data
public class LangTextObjectModel {
    private String name;
    private ArrayList<LangComponent> components;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (LangComponent component:components) {
            builder.append(component);
            builder.append("\n");
        }

        return "LangTextObjectModel\n" +
                "name=" + name + '\n' +
                "components=\n" + builder.toString() +
                "----";
    }
}
