import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.apache.poi.ss.usermodel.CellStyle.BORDER_THICK;

public class TextProcessing {
    private static Logger LOGGER = Logger.getLogger(TextProcessing.class);

    public ArrayList<String> processJsFile(FileInputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));

        ArrayList<String> builder = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {

            builder.add(line);
        }
        return builder;
    }

    public void processLangs(ArrayList<LangTextFileModel> textModels) {
        for (LangTextFileModel fileModel : textModels) {
            processFileModel(fileModel);
        }
    }

    /**
     * processFileModel разбивает текст на блоки перевода и обрабатывает каждый блок по отдельности
     * @param langTextFileModel модель перевода
     */
    public void processFileModel(LangTextFileModel langTextFileModel) {
        ArrayList<String> blockList = null;
        for (String s : langTextFileModel.getText()) {

            s = s.trim();
            if (s.contains("Ext.define") && s.charAt(0) == 'E') {
                if (blockList != null)
                    processBlock(blockList, langTextFileModel.getTranslations());
                blockList = new ArrayList<>();
            }
            if (blockList != null)
                blockList.add(s);
        }
        // последний блок
        if(blockList!=null&&blockList.size()>0)
        {
            processBlock(blockList, langTextFileModel.getTranslations());
        }
    }

    /**
     * processBlock обрабатывает перевод одного компонента
     * @param block необрабоатанный блок перевода
     * @param translations карта всего перевода
     */
    private void processBlock(ArrayList<String> block, HashMap<String, HashMap<String, String>> translations) {
        LangTextObjectModel textObjectModel = new LangTextObjectModel();
        ArrayList<LangComponent> components = new ArrayList<>();
        textObjectModel.setComponents(components);
        HashMap<String, String> blockTranslations = new HashMap<>();

        String nameStr = block.get(0);
        String blockName = nameStr.substring(nameStr.indexOf("\"") + 1, nameStr.lastIndexOf("\""));
        blockName = blockName.substring(blockName.indexOf(".") + 1);
        blockName = blockName.substring(blockName.indexOf(".") + 1);
        blockName = blockName.substring(blockName.indexOf(".") + 1);
        textObjectModel.setName(blockName);
//        System.out.println(blockName);
        block = cleanBlock(block);
        for (String s : block) {
            String[] split = s.split(":");
            LangComponent component = new LangComponent(split[0].trim(), split[1].trim());
            components.add(component);
            blockTranslations.put(split[0].trim(), split[1].trim());
        }
        translations.put(textObjectModel.getName(), blockTranslations);

//        return textObjectModel;
    }

    private ArrayList<String> cleanBlock(ArrayList<String> block) {
        ArrayList<String> cleanList = new ArrayList<>();
        for (int i = 0; i < block.size(); i++) {
            String s = block.get(i).trim();
            if (s.length() > 3 && s.contains(":") && !s.contains("override"))
                cleanList.add(s);
        }

        return cleanList;
    }

    public HSSFWorkbook generateExcel(ArrayList<LangTextFileModel> langTextFileModels) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();
        short thinStyle = 1;
        short mediumStyle = 2;
        int curRow = 1;
        HSSFCellStyle styleThin = createWbStyle(thinStyle, wb);
        HSSFCellStyle styleThin_Color = createWbStyle(thinStyle, wb);
        styleThin_Color.setFillForegroundColor(HSSFColor.LIME.index);
        styleThin_Color.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        HSSFCellStyle styleMedium = createWbStyle(mediumStyle, wb);
        HSSFCellStyle styleFat = createWbStyle(BORDER_THICK, wb);

        for (int i = 0; i < langTextFileModels.size(); i++) {
            if (langTextFileModels.get(i).getFileLangName().contains("ru")) {
                if (i != 0)
                    Collections.swap(langTextFileModels, 0, i);
                break;
            }
        }
        LangTextFileModel ruModel = langTextFileModels.get(0);
        java.util.List<LangTextFileModel> nonRuLangs = langTextFileModels.subList(1, langTextFileModels.size());
        //заголовок таблицы
        Row row = sheet.createRow(0);
        for (int i = 0; i < langTextFileModels.size(); i++) {

            Cell cell = row.createCell(i + 1);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellStyle(styleMedium);

            cell.setCellValue(langTextFileModels.get(i).getFileLangName());

        }
        HashMap<String, HashMap<String, String>> translationRu = ruModel.getTranslations();

        // запоняем перевод:
        for (String blockName : translationRu.keySet()) {
            row = sheet.createRow(curRow);
            cellCreation(row, 0, styleFat, blockName);
            curRow++;

            HashMap<String, String> ruTranslate = translationRu.get(blockName);
            for (String elName : ruTranslate.keySet()) {

                row = sheet.createRow(curRow);
                cellCreation(row, 0, styleThin, elName);
                cellCreation(row, 1, styleThin, ruTranslate.get(elName));


                for (int k = 0; k < nonRuLangs.size(); k++) {

                    HashMap<String, HashMap<String, String>> translation = nonRuLangs.get(k).getTranslations();
                    String tr = findTranslation(translation, blockName, elName);
                    if (!tr.isEmpty())
                        cellCreation(row, k + 2, styleThin, tr);
                    else
                        cellCreation(row, k + 2, styleThin_Color,"!!!!"+ elName);
                }

                curRow++;
            }

        }

        for (int i = 0; i < langTextFileModels.size() + 1; i++)
            sheet.autoSizeColumn(i);
        return wb;
    }

    private String findTranslation(HashMap<String, HashMap<String, String>> translation, String blockName, String elName) {
        String res = "";
        if (translation.get(blockName) != null && translation.get(blockName).get(elName) != null)
            res = translation.get(blockName).get(elName);
        return res;
    }

    private void cellCreation(Row row, int column, HSSFCellStyle style, String value) {
        Cell cell = row.createCell(column);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }

    private HSSFCellStyle createWbStyle(Short style, HSSFWorkbook wb) {
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setBorderBottom(style);
        cellStyle.setBorderRight(style);
        cellStyle.setBorderTop(style);
        cellStyle.setBorderLeft(style);
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        return cellStyle;

    }

    public ArrayList<LangTextFileModel> readTranslateFromExcel(FileInputStream excelFile) throws IOException, InvalidFormatException {
        LOGGER.debug("Excel file read!");
        Workbook workbook;
        ArrayList<LangTextFileModel> textFileModels = new ArrayList<>();
        workbook = WorkbookFactory.create(excelFile);
        Sheet sheet = workbook.getSheetAt(0);
        excelFile.close();
        int coulumnCount = sheet.getRow(0).getLastCellNum();
        LOGGER.debug("Columns in lang file:" + coulumnCount);
        for (int i = 1; i < coulumnCount; i++) {
            LangTextFileModel langModel= new LangTextFileModel();
            langModel.setFileLangName(sheet.getRow(0).getCell(i).getStringCellValue());
            HashMap<String,HashMap<String,String>> translation = new HashMap<>();

            for (int k=1;k<sheet.getLastRowNum();k++)
            {
                Cell blockNameCell= sheet.getRow(k).getCell(0);
                HashMap<String,String> blockTranslate = new HashMap<>();
                if(blockNameCell==null)
                    break;
                while(sheet.getRow(k+1)!=null&&sheet.getRow(k+1).getCell(i)!=null)
                {
                    blockTranslate.put(sheet.getRow(k+1).getCell(0).getStringCellValue(),sheet.getRow(k+1).getCell(i).getStringCellValue());
                    k++;
                }
                translation.put(blockNameCell.getStringCellValue(),blockTranslate);
            }
            langModel.setTranslations(translation);
            textFileModels.add(langModel);
        }
        return textFileModels;
    }

    public String prepareLangTextToSave(LangTextFileModel donor, LangTextFileModel newTranslate) {

        String langFileName=newTranslate.getFileLangName();
        String lang=langFileName.substring(langFileName.lastIndexOf("-")+1,langFileName.lastIndexOf("."));
//        for (String blockName:newTranslate.getTranslations().keySet()) {
//            System.out.println("---------"+blockName);
//            for (String tr:newTranslate.getTranslations().get(blockName).keySet()) {
//                System.out.println(tr+":"+newTranslate.getTranslations().get(blockName).get(tr));
//            }
//        }

//        return "";
        newTranslate.setText((ArrayList<String>) donor.getText().clone());

        Set<String> blockNames= newTranslate.getTranslations().keySet();

        for (String blockName:blockNames) {
//            System.out.println("----blockName:"+blockName);
            for(int i=0;i<newTranslate.getText().size();i++)
            {
                String line= newTranslate.getText().get(i);
                int lastLineindex=i;
                // ищем блок
                if(line.contains("."+blockName.trim())) {
                    for(int k=i+1;k<newTranslate.getText().size();k++)
                    {
                        if(newTranslate.getText().get(k).contains("Ext.define"))
                        {
                            lastLineindex=k;
                            break;
                        }
                    }
                    if(lastLineindex==i&&i!=newTranslate.getText().size()-1)
                        lastLineindex=newTranslate.getText().size()-1;

                    HashMap<String, String> blockTranslation = newTranslate.getTranslations().get(blockName);
                    for (String trname:blockTranslation.keySet()) {
                        for (int k=i+1;k<lastLineindex;k++)
                        {
                            if(newTranslate.getText().get(k).matches ("^\\s*"+trname.trim()+"\\s*:.*"))
                            {
                                newTranslate.getText().set(k,trname+":"+checkTranslate(blockTranslation.get(trname),newTranslate.getText().get(k+1)));
                                break;
                            }
                        }
                    }

                }
            }
        }
        StringBuilder result =new StringBuilder();
        for (String s:newTranslate.getText()) {
            if(s.contains("Ext.define"))
                s=s.replaceAll(".ru.","."+lang+".");
            result.append(s).append("\n");
        }
//        System.out.println(result.toString());
        return result.toString();
    }
    public String checkTranslate(String input,String next)
    {   input=input.trim();
        if(input.length()<4)
        return input;
        if (input.isEmpty())
            return "'',";
        if(input.charAt(0)!='\''&&input.charAt(0)!='"'&&!input.matches("^\\s*Ext..*"))
            input="'"+input;
        if(input.charAt(input.length()-1)!=',')
            input=input+",";
        if(input.charAt(input.length()-2)!='\''&&input.charAt(input.length()-2)!='"')
            input=input.substring(0,input.length()-2)+"',";

        if(next.contains("});"))
            input=input.substring(0,input.length()-1);

        return input;
    }
}
