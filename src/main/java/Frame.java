import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

public class Frame extends JFrame implements WindowListener, ActionListener {
    private static Logger LOGGER = Logger.getLogger(Frame.class);

    private JPanel panel;
    private List list;
    private JFileChooser dirChooser;
    private JLabel label;
    private JLabel label2;
    private JLabel label3;
    private JButton button;
    private JButton button2;
    private JButton button3;

    public Frame() throws HeadlessException {

        panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        list = new List(20);
        dirChooser = new JFileChooser();
        list.setVisible(true);
        label = new JLabel("Выбрать папку");
        label2 = new JLabel("");
        label3 = new JLabel("Обратный импорт");
        button = new JButton("Открыть");
        button.addActionListener(this);
        button2 = new JButton("Обработать");
        button2.addActionListener(this);
        button2.setEnabled(false);

        button3 = new JButton("Импорт XLS");
        button3.addActionListener(this);


        panel.add(button);
        panel.add(label);
        panel.add(button2);
        panel.add(label2);
        panel.add(button3);
        panel.add(label3);
        add(list);
        add(panel, BorderLayout.NORTH);
        addWindowListener(this);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(button)) {
            try {
                dirButton(false);
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(null, "Ошибка обработки файла", "Ошибка", JOptionPane.INFORMATION_MESSAGE);
                e1.printStackTrace();
            }
        }
        if (e.getSource().equals(button2)) {
            try {
                processFiles();
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(null, "Ошибка обработки файла", "Ошибка", JOptionPane.INFORMATION_MESSAGE);
                e1.printStackTrace();
            }
        }
        if (e.getSource().equals(button3)) {
            try {
                dirButton(true);
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(null, "Ошибка обработки файла", "Ошибка", JOptionPane.INFORMATION_MESSAGE);
                e1.printStackTrace();
            }
        }
    }

    public void windowOpened(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }

    private void dirButton(boolean backImport) throws IOException, InvalidFormatException {
        dirChooser.removeChoosableFileFilter(null);
        if (backImport)
            dirChooser.setDialogTitle("Открыть папку с ext-lang-ru.js и excel файлом с переводом");
        else
            dirChooser.setDialogTitle("Открыть папку с js");

        dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dirChooser.setCurrentDirectory(new File("."));
        if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            list.removeAll();
            setTitle(dirChooser.getSelectedFile().getPath());
            File choosenDir = new File(dirChooser.getSelectedFile().getPath());
            String[] Flist = choosenDir.list();
            if (Flist != null) {
                for (String S : Flist) {
                    int index = S.lastIndexOf('.');
                    if (index != -1) {
                        String ext = S.substring(index);
                        if (ext.toLowerCase().equals(".js")||(backImport&&(ext.toLowerCase().equals(".xls")||ext.toLowerCase().equals(".xlsx"))))
                            list.add(S);
                    }
                }
                if (list.getItemCount() > 0 && backImport)
                    backImport();

                if (list.getItemCount() > 0)
                    button2.setEnabled(true);
                else
                    button2.setEnabled(false);
            } else
                JOptionPane.showMessageDialog(null, "Выбранная папаке не содержит PDF файлов", "Информация", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void processFiles() throws IOException {
        ArrayList<LangTextFileModel> langTextModels = new ArrayList<>();
        TextProcessing textProcessing = new TextProcessing();
        while (list.getItemCount() > 0) {
            int index = 0;
            LOGGER.debug("---------------------------------------------------------------------------------------------------------------");
            LOGGER.debug("File N:" + Integer.toString(index + 1));
            LOGGER.debug("File processing:" + getTitle() + File.separator + list.getItem(0));
            try {
                FileInputStream jsFile = new FileInputStream(new File(getTitle() + File.separator + list.getItem(index)));
                langTextModels.add(new LangTextFileModel(list.getItem(index), textProcessing.processJsFile(jsFile), new HashMap<String, HashMap<String, String>>()));

                jsFile.close();
            } catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "Ошибка при обработке файла :" + list.getItem(index), "Ошибка", JOptionPane.ERROR_MESSAGE);
                LOGGER.error("Error processing PDF file:" + getTitle() + File.separator + list.getItem(index));
                exc.printStackTrace();
                return;
            }
            list.remove(0);
            index++;
        }
        textProcessing.processLangs(langTextModels);
        FileOutputStream outputStream = new FileOutputStream(getTitle() + File.separator + "out.xls");
        textProcessing.generateExcel(langTextModels).write(outputStream);
        outputStream.close();
//        System.out.println(langTextModels);

    }

    private void backImport() throws IOException, InvalidFormatException {
        LOGGER.debug("back import");
        ArrayList<LangTextFileModel> langTextModels = new ArrayList<>();

        TextProcessing textProcessing = new TextProcessing();
        if (list.getItemCount() > 0 && Arrays.asList(list.getItems()).contains("ext-lang-ru.js")) {
            FileInputStream jsFile = new FileInputStream(new File(getTitle() + File.separator + "ext-lang-ru.js"));
            LOGGER.debug("ext-lang-ru.js FOUND");
            LangTextFileModel ruFile =
                    new LangTextFileModel("ext-lang-ru.js", textProcessing.processJsFile(jsFile), new HashMap<String, HashMap<String, String>>());
            jsFile.close();
            textProcessing.processFileModel(ruFile);
            LOGGER.debug("ext-lang-ru.js PROCESSED");

            for (String file:list.getItems()) {
                Optional<String> ext= Optional.ofNullable(file)
                        .filter(f -> f.contains(".xls")||f.contains(".xlsx"))
                        .map(f -> f.substring(file.lastIndexOf(".") + 1));

                if(ext.isPresent())
                {
                    LOGGER.debug("Found Excel:"+file);
                    FileInputStream excelFile = new FileInputStream(new File(getTitle() + File.separator + file));
                    ArrayList<LangTextFileModel> fileModels= textProcessing.readTranslateFromExcel(excelFile);
                    File outDir=new File(getTitle() + File.separator+"out");
                    if(!outDir.exists())
                    {
                        outDir.mkdir();
                    }
                    for (LangTextFileModel fileModel:fileModels) {
                        String fileText=textProcessing.prepareLangTextToSave(ruFile,fileModel);
                        FileOutputStream outputStream = new FileOutputStream(getTitle() + File.separator+"out"+File.separator + fileModel.getFileLangName());
                        outputStream.write(fileText.getBytes());
                        outputStream.close();
                    }
                    break;
                }
            }

        }
    }

//    public ArrayList<String> processJsFile(FileInputStream stream) throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
//
//        ArrayList<String> builder = new ArrayList<>();
//        String line;
//        while ((line = reader.readLine()) != null) {
//
//            builder.add(line);
//        }
//        return builder;
//    }
//
//    private void processLangs(ArrayList<LangTextFileModel> textModels) {
//        for (LangTextFileModel fileModel : textModels) {
//            processFileModel(fileModel);
//        }
//    }
//
//    private void processFileModel(LangTextFileModel langTextFileModel) {
//        ArrayList<String> blockList = null;
//        for (String s : langTextFileModel.getText()) {
//
//            s = s.trim();
//            if (s.contains("Ext.define") && s.charAt(0) == 'E') {
//                if (blockList != null)
//                    processBlock(blockList, langTextFileModel.getTranslations());
////                    langTextFileModel.getObjectModels().add(processBlock(blockList,langTextFileModel.getTranslations()));
//                blockList = new ArrayList<>();
//            }
//            if (blockList != null)
//                blockList.add(s);
//        }
//    }
//
//    private void processBlock(ArrayList<String> block, HashMap<String, HashMap<String, String>> translations) {
//        LangTextObjectModel textObjectModel = new LangTextObjectModel();
//        ArrayList<LangComponent> components = new ArrayList<>();
//        textObjectModel.setComponents(components);
//        HashMap<String, String> blockTranslations = new HashMap<>();
//
//        String nameStr = block.get(0);
//        String blockName=nameStr.substring(nameStr.indexOf("\"") + 1, nameStr.lastIndexOf("\""));
//        blockName=blockName.substring(blockName.indexOf(".")+1);
//        blockName=blockName.substring(blockName.indexOf(".")+1);
//        blockName=blockName.substring(blockName.indexOf(".")+1);
//        textObjectModel.setName(blockName);
//        block = cleanBlock(block);
//        for (String s : block) {
//            String[] split = s.split(":");
//            LangComponent component = new LangComponent(split[0].trim(), split[1].trim());
//            components.add(component);
//            blockTranslations.put(split[0].trim(), split[1].trim());
//        }
//        translations.put(textObjectModel.getName(), blockTranslations);
//
////        return textObjectModel;
//    }
//
//    private ArrayList<String> cleanBlock(ArrayList<String> block) {
//        ArrayList<String> cleanList = new ArrayList<>();
//        for (int i = 0; i < block.size(); i++) {
//            String s = block.get(i).trim();
//            if (s.length() > 3 && s.contains(":") && !s.contains("override"))
//                cleanList.add(s);
//        }
//
//        return cleanList;
//    }
//
//    private HSSFWorkbook generateExcel(ArrayList<LangTextFileModel> langTextFileModels) {
//        HSSFWorkbook wb = new HSSFWorkbook();
//        HSSFSheet sheet = wb.createSheet();
//        short thinStyle = 1;
//        short mediumStyle = 2;
//        short fatStyle = BORDER_THICK;
//        int curRow=1;
//        HSSFCellStyle styleThin = createWbStyle(thinStyle, wb);
//        HSSFCellStyle styleMedium = createWbStyle(mediumStyle,wb);
//        HSSFCellStyle styleFat = createWbStyle(fatStyle,wb);
//
//        for (int i = 0; i < langTextFileModels.size(); i++) {
//            if (langTextFileModels.get(i).getFileLangName().contains("ru")) {
//                if (i != 0)
//                    Collections.swap(langTextFileModels, 0, i);
//                break;
//            }
//        }
//        LangTextFileModel ruModel = langTextFileModels.get(0);
//        java.util.List<LangTextFileModel> nonRuLangs= langTextFileModels.subList(1,langTextFileModels.size());
//        //заголовок таблицы
//        Row row = sheet.createRow(0);
//        for (int i = 0; i < langTextFileModels.size(); i++) {
//
//            Cell cell = row.createCell(i+1);
//            cell.setCellType(Cell.CELL_TYPE_STRING);
//            cell.setCellStyle(styleMedium);
//
//            cell.setCellValue(langTextFileModels.get(i).getFileLangName());
//
//        }
//        HashMap<String,HashMap<String,String>> translationRu=ruModel.getTranslations();
//
//        // запоняем перевод:
//        for (String blockName: translationRu.keySet()) {
//            row = sheet.createRow(curRow);
//            cellCreation(row,0,styleFat,blockName);
//            curRow++;
//
//            HashMap<String,String> ruTranslate= translationRu.get(blockName);
//            for (String elName: ruTranslate.keySet()) {
//
//                row = sheet.createRow(curRow);
//                cellCreation(row,0,styleThin,elName);
//                cellCreation(row,1,styleThin,ruTranslate.get(elName));
//
//
//                for (int k=0;k<nonRuLangs.size();k++) {
//
//                    HashMap<String,HashMap<String,String>> translation=nonRuLangs.get(k).getTranslations();
//                    String tr=findTranslation(translation,blockName,elName);
//                    if(!tr.isEmpty())
//                        cellCreation(row,k+2,styleThin,tr);
//                }
//
//                curRow++;
//            }
//
//        }
//
//        for (int i = 0; i < langTextFileModels.size()+1; i++)
//            sheet.autoSizeColumn(i);
//        return wb;
//    }
//
//    private String findTranslation(HashMap<String,HashMap<String,String>> translation,String blockName, String elName)
//    {
//        String res="";
//        if(translation.get(blockName)!=null&&translation.get(blockName).get(elName)!=null)
//            res=translation.get(blockName).get(elName);
//        return res;
//    }
//
//    private void cellCreation(Row row,int column,HSSFCellStyle style,String value)
//    {
//        Cell cell = row.createCell(column);
//        cell.setCellType(Cell.CELL_TYPE_STRING);
//        cell.setCellValue(value);
//        cell.setCellStyle(style);
//    }
//
//    private HSSFCellStyle createWbStyle(Short style, HSSFWorkbook wb) {
//        HSSFCellStyle cellStyle = wb.createCellStyle();
//        cellStyle.setBorderBottom(style);
//        cellStyle.setBorderRight(style);
//        cellStyle.setBorderTop(style);
//        cellStyle.setBorderLeft(style);
//        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
//        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
//        return cellStyle;
//
//    }
}
