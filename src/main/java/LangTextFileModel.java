import com.sun.jmx.remote.internal.ArrayQueue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LangTextFileModel {

    private String fileLangName;
    private ArrayList<String> text;
//    private ArrayList<LangTextObjectModel>objectModels;
    private HashMap<String,HashMap<String,String>> translations;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Set<String> blockNames= translations.keySet();

        for (String blockName:blockNames) {
            builder.append("--Block:").append(blockName).append(":\n");
            HashMap<String,String> translation = translations.get(blockName);
            for(String elName:translation.keySet())
            {
                builder.append(elName).append(":").
                        append(translation.get(elName)).append("\n");
            }

        }

        return "LangTextFileModel{" +
                ", fileLangName=" + fileLangName +
                ", translations=" + builder.toString() +
                '}';
    }
}
