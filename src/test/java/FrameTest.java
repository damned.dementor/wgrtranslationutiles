import org.junit.Test;

import java.util.Optional;

public class FrameTest {

    @Test
    public void test()
    {
        String filename="file.ext";
        Optional<String> stringOptiona= Optional.ofNullable(filename)
                .filter(s -> s.contains("."))
                .map(s -> s.substring(filename.lastIndexOf(".")+1));
        System.out.println(stringOptiona.get());
    }

    @Test
    public void test2()
    {
        String test="    displayText           :";
        String test2="displayText";
        System.out.println(test.matches("^\\s*"+test2.trim()+"\\s*:"));


        test="    treeInstr   :'Инструкция',";
        test2="treeInstr";
        System.out.println(test.matches("^\\s*"+test2.trim()+"\\s*:.*"));


        test="    treeInstr   :'Инструкция,";
        if(test.charAt(test.length()-2)!='\''||test.charAt(test.length()-2)!='"')
            test=test.substring(0,test.length()-2)+"',";
        System.out.println(test);
    }

    @Test
    public void test3()
    {
        String input=" 'Dieses Feld hat die E-Mail Adresse im Format zu enthalten \"user@example.com\"',";
        TextProcessing textProcessing = new TextProcessing();
        System.out.println(textProcessing.checkTranslate(input,""));
    }

    @Test
    public void test4()
    {
        String test="ext-lang-ruewrwerwer.js";
        System.out.println(test.substring(test.lastIndexOf("-")+1,test.lastIndexOf(".")));
    }

    @Test
    public void test5()
    {
        String input="   Ext.baseCSSPrefix + 'html-editor-tip'";
        System.out.println(input.matches("^\\s*Ext..*"));
    }
}